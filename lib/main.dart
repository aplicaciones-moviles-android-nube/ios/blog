import 'package:flutter/cupertino.dart';
import 'package:blog/pages/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const CupertinoApp(
      title: 'Flutter Demo',
      theme:  CupertinoThemeData(
      ),
      home:  HomePage(),
      debugShowCheckedModeBanner: false,
     
    );
  }
}

