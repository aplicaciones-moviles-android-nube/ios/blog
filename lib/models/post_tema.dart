class PostTema {
  final int id;
  final int postId;
  final int temaId;

  PostTema({
    required this.id,
    required this.postId,
    required this.temaId,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'postId': postId,
      'temaId': temaId,
    };
  }

  factory PostTema.fromJson(Map<String, dynamic> json) {
    return PostTema(
      id: json['id'] ??
          0, // Si 'id' es null, asigna 0 como valor predeterminado
      postId: json['postId'] ??
          0, // Si 'postId' es null, asigna 0 como valor predeterminado
      temaId: json['temaId'] ??
          0, // Si 'temaId' es null, asigna 0 como valor predeterminado
    );
  }
}
