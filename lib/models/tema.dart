class Tema {
  final int id;
  final String nombre;

  Tema({
    required this.id,
    required this.nombre,
  });

  factory Tema.fromJson(Map<String, dynamic> json) {
    return Tema(
      id: json['id'],
      nombre: json['nombre'],
    );
  }
}
