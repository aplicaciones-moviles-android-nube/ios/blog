//Modelo Post

class Post {
  final int id;
  final String titulo;
  final String contenido;
  final String url;
  final String fechaPublicacion;
  final int ranking;
  final int autor; 

//constructor Post que acepta valores por cada uno de los atributos del post
  Post({
    required this.id,
    required this.titulo,
    required this.contenido,
    required this.url,
    required this.fechaPublicacion,
    required this.ranking,
    required this.autor,
  });


  //Metodo toma el mapa JSON como argumento y extrae cada atributo del post
  factory Post.fromJson(Map<String, dynamic> json) {  //Map es una estructura de datos que asocia claves de tipo String con valores de cualquier tipo
  return Post(
    id: json['id'],
    titulo: json['titulo'],
    contenido: json['contenido'],
    url: json['url'],
    fechaPublicacion: json['fechaPublicacion'],
    autor: json['autor']['id'], 
    ranking: json['ranking'],
  );
}

}
