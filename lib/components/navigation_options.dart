import 'package:blog/pages/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:blog/pages/articles.dart';
import 'package:blog/services/api_services.dart';
import 'package:blog/models/post.dart';
import 'package:blog/pages/aboutme.dart';
import 'package:blog/pages/home_page.dart';

class NavigationOptions extends StatelessWidget {
  const NavigationOptions({super.key});

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      padding: EdgeInsets.zero,
      onPressed: () {
        // Mostrar el menú de acciones
        showCupertinoModalPopup(
          context: context,
          builder: (BuildContext context) {
            return CupertinoActionSheet(
              title: const Text('Menú'),
              actions: <Widget>[
                CupertinoActionSheetAction(
                  onPressed: () {
                    // Acción para "Home"
                    Navigator.pushReplacement(
                      context,
                      CupertinoPageRoute(
                          builder: (context) => const HomePage()),
                    );

                    // Eliminar todas las rutas de navegación anteriores
                    Navigator.of(context).popUntil((route) => route.isFirst);
                  },
                  child: const Text('Home'),
                ),
                CupertinoActionSheetAction(
                  onPressed: () {
                    // Acción para "Acerca de mí"
                    Navigator.of(context).push(CupertinoPageRoute(
                        builder: (context) => const AboutMePage()));
                  },
                  child: const Text('Acerca de mí'),
                ),
                CupertinoActionSheetAction(
                  onPressed: () {
                    // Acción para "Artículos"
                    _goToArticles(context);
                  },
                  child: const Text('Artículos'),
                ),
                CupertinoActionSheetAction(
                  onPressed: () {
                    // Acción para "login"
                    Navigator.of(context).push(CupertinoPageRoute(
                        builder: (context) => const LoginPage()));
                  },
                  child: const Text('Login'),
                ),
              ],
              cancelButton: CupertinoActionSheetAction(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text('Cancelar'),
              ),
            );
          },
        );
      },
      child: const Icon(CupertinoIcons.ellipsis),
    );
  }

  void _goToArticles(BuildContext context) async {
    try {
      // Obtener el futuro de los artículos
      Future<List<Post>> postsFuture = ApiService.getAllPosts();

      // Navegar al ArticlePage y pasar los datos de los artículos como argumento
      Navigator.of(context).push(
        CupertinoPageRoute(
          builder: (context) => ArticlePage(postsFuture: postsFuture),
        ),
      );
    } catch (e) {
      // Imprimir el error en la consola
      // print("Error al obtener los artículos: $e");
      // Aquí puedes mostrar un diálogo de error o realizar cualquier otra acción
    }
  }
}
