import 'package:flutter/cupertino.dart';

class BarraInferior extends StatelessWidget {
  final Widget icono;
  final Widget? trailing;
  final Function()? onTapHome;
  final Function()? onTapGroup;

  const BarraInferior({
    super.key,
    required this.icono,
    this.trailing,
    this.onTapHome,
    this.onTapGroup,
  }) ;

  @override
  Widget build(BuildContext context) {
    return CupertinoNavigationBar(
      backgroundColor: CupertinoColors.white, // Cambiar el color de fondo a blanco
      border: null, // Eliminar la línea en la parte inferior
      middle: Row(
        children: [
          GestureDetector(
            onTap: onTapHome, // Llama onTapHome si está definido
            child: icono, // Ícono de inicio
          ),
          const Spacer(), // Espaciador flexible para mover el ícono al final
          if (trailing != null) trailing!, // Widget opcional al final
          if (onTapGroup != null)
            GestureDetector(
              onTap: onTapGroup, // Llama onTapGroup si está definido
              child: const Icon(
                CupertinoIcons.group,
                size: 32, // Tamaño del ícono del grupo
              ),
            ),
        ],
      ),
    );
  }
}
