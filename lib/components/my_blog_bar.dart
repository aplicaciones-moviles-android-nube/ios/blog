import 'package:flutter/cupertino.dart';

class MyBlogBar extends CupertinoNavigationBar {
  const MyBlogBar({
    Key? key,
    required Widget middle,
    Widget? trailing, // Agregar el widget de opciones aquí
    Color? backgroundColor,
    Border? border,
    EdgeInsetsDirectional padding = EdgeInsetsDirectional.zero,
    Object? heroTag,
    double? previousPageTitleOpacity,
    TransitionBuilder? transitionBetweenRoutes,
  }) : super(
          key: key,
          middle: middle,
          trailing: trailing, // Asignar el widget de opciones
          backgroundColor: backgroundColor,
          border: border,
          padding: padding,
         
        );
}
