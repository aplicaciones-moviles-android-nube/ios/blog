import 'package:flutter/cupertino.dart';
import 'package:blog/services/api_services.dart';
import 'package:blog/models/tema.dart';

class AssignTopicPage extends StatefulWidget {
  const AssignTopicPage({super.key});

  @override
  AssignTopicPageState createState() => AssignTopicPageState();
}

class AssignTopicPageState extends State<AssignTopicPage> {
  final TextEditingController _postIdController = TextEditingController();
  Tema? _selectedTopic;
  List<Tema> _temas = [];

  Future<void> _assignTopic() async {
    if (_selectedTopic == null || _postIdController.text.isEmpty) {
      // Si el tema o el ID del post no están seleccionados, mostrar un mensaje de error
      showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: const Text('Error'),
            content: const Text('Por favor, seleccione un tema y proporcione un ID de post.'),
            actions: [
              CupertinoDialogAction(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
      return;
    }

    try {
      await ApiService.assignTopic(
        int.parse(_postIdController.text), // ID del post
         _selectedTopic!.id, // ID del tema
      );

      // Mostrar un diálogo con el mensaje correspondiente según el resultado
      showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: const Text('Asignación exitosa'),
            content: const Text('El tema se asignó correctamente al post.'),
            actions: [
              CupertinoDialogAction(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                  // Si la asignación fue exitosa, cierra la página actual
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } catch (e) {
      // Mostrar un diálogo con el mensaje de error
      showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: const Text('Error al asignar el tema al post'),
            content: const Text('Hubo un error al intentar asignar el tema al post.'),
            actions: [
              CupertinoDialogAction(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Asignar Tema'),
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (_temas.isNotEmpty)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text('Seleccionar Tema'),
                    CupertinoPicker(
                      itemExtent: 50.0,
                      diameterRatio: 1.5,
                      looping: false,
                      onSelectedItemChanged: (index) {
                        setState(() {
                          _selectedTopic = _temas[index];
                        });
                      },
                      children: _temas.map((tema) {
                        return Text(tema.nombre);
                      }).toList(),
                    ),
                  ],
                ),
              CupertinoFormRow(
                child: CupertinoTextField(
                  controller: _postIdController,
                  keyboardType: TextInputType.number,
                  placeholder: 'ID del Post',
                ),
              ),
              const SizedBox(height: 32.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  CupertinoButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text('Cancelar'),
                  ),
                  CupertinoButton(
                    onPressed: _assignTopic,
                    child: const Text('Guardar'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadThemes();
  }

  Future<void> _loadThemes() async {
    try {
      // Obtener la lista de temas
      List<Tema> temas = await ApiService.getThemes();

      // Agregar el elemento "Seleccionar Tema" al principio de la lista
      temas.insert(0, Tema(id: -1, nombre: 'Seleccionar Tema'));

      // Actualizar el estado con la lista modificada
      setState(() {
        _temas = temas;
      });
    } catch (e) {
      // Mostrar un diálogo con el mensaje de error
      showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: const Text('Error al cargar los temas'),
            content: const Text('Hubo un error al cargar la lista de temas.'),
            actions: [
              CupertinoDialogAction(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }
}
