import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart'; 
import 'package:blog/pages/login.dart';
import 'package:blog/services/api_services.dart';

class CreateAccountPage extends StatefulWidget {
  const CreateAccountPage({super.key});

  @override
  CreateAccountPageState createState() => CreateAccountPageState();
}

class CreateAccountPageState extends State<CreateAccountPage> {
  final TextEditingController _proveedorController = TextEditingController();
  final TextEditingController _proveedorIdController = TextEditingController();
  final TextEditingController _nombreController = TextEditingController();
  final TextEditingController _correoElectronicoController =
      TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Crear cuenta'),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          child: ListView(
            children: [
              CupertinoTextField(
                controller: _proveedorController,
                placeholder: 'Proveedor',
                keyboardType: TextInputType.text,
              ),
              const SizedBox(height: 10),
              CupertinoTextField(
                controller: _proveedorIdController,
                placeholder: 'ID del Proveedor',
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  // Utiliza TextInputFormatter para permitir solo dígitos
                  FilteringTextInputFormatter.digitsOnly,
                ],
              ),
              const SizedBox(height: 10),
              CupertinoTextField(
                controller: _nombreController,
                placeholder: 'Nombre',
                keyboardType: TextInputType.text,
              ),
              const SizedBox(height: 10),
              CupertinoTextField(
                controller: _correoElectronicoController,
                placeholder: 'Correo Electrónico',
                keyboardType: TextInputType.emailAddress,
              ),
              const SizedBox(height: 10),
              CupertinoTextField(
                controller: _passwordController,
                placeholder: 'Contraseña',
                keyboardType: TextInputType.text,
                obscureText: true,
              ),
              const SizedBox(height: 20),
              CupertinoButton.filled(
                onPressed: () async {
                  if (_proveedorController.text.isEmpty ||
                      _proveedorIdController.text.isEmpty ||
                      _nombreController.text.isEmpty ||
                      _correoElectronicoController.text.isEmpty ||
                      _passwordController.text.isEmpty) {
                    // Si alguno de los campos está vacío, mostrar un mensaje de error
                    showCupertinoDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return CupertinoAlertDialog(
                          title: const Text('Error'),
                          content: const Text('Por favor, complete todos los campos.'),
                          actions: [
                            CupertinoDialogAction(
                              child: const Text('OK'),
                              onPressed: () {
                                Navigator.of(context).pop(); // Cerrar el diálogo
                              },
                            ),
                          ],
                        );
                      },
                    );
                  } else if (!isNumeric(_proveedorIdController.text)) {
                    // Si el ID del proveedor no es numérico, mostrar un mensaje de error
                    showCupertinoDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return CupertinoAlertDialog(
                          title: const Text('Error'),
                          content: const Text('El ID del proveedor debe ser un número.'),
                          actions: [
                            CupertinoDialogAction(
                              child: const Text('OK'),
                              onPressed: () {
                                Navigator.of(context).pop(); // Cerrar el diálogo
                              },
                            ),
                          ],
                        );
                      },
                    );
                  } else {
                    // Todos los campos están llenos y el ID del proveedor es numérico
                    final proveedor = _proveedorController.text;
                    final proveedorId = int.parse(_proveedorIdController.text);
                    final nombre = _nombreController.text;
                    final correoElectronico = _correoElectronicoController.text;
                    final password = _passwordController.text;

                    // Crear un mapa con los datos del nuevo usuario
                    final newUser = {
                      'proveedor': proveedor,
                      'proveedorId': proveedorId,
                      'nombre': nombre,
                      'correoElectronico': correoElectronico,
                      'password': password,
                    };

                    try {
                      // Llamar al servicio para crear el nuevo usuario
                      bool success = await ApiService.createNewUser(newUser);
                      if (success) {
                        // El usuario se creó correctamente, muestra un diálogo
                        showCupertinoDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return CupertinoAlertDialog(
                              title: const Text('Éxito'),
                              content: const Text('Usuario creado exitosamente'),
                              actions: [
                                CupertinoDialogAction(
                                  child: const Text('OK'),
                                  onPressed: () {
                                    Navigator.pushReplacement(
                                      context,
                                      CupertinoPageRoute(builder: (context) => const LoginPage()),
                                    );
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      } else {
                        // Hubo un error al crear el usuario
                        showCupertinoDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return CupertinoAlertDialog(
                              title: const Text('Error'),
                              content: const Text('Hubo un error al crear el usuario'),
                              actions: [
                                CupertinoDialogAction(
                                  child: const Text('OK'),
                                  onPressed: () {
                                    Navigator.of(context).pop(); // Cerrar el diálogo
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      }
                    } catch (e) {
                      // Manejar cualquier excepción que ocurra durante la creación del usuario
                      showCupertinoDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return CupertinoAlertDialog(
                            title: const Text('Error'),
                            content: Text('Hubo un error al crear el usuario: $e'),
                            actions: [
                              CupertinoDialogAction(
                                child: const Text('OK'),
                                onPressed: () {
                                  Navigator.of(context).pop(); // Cerrar el diálogo
                                },
                              ),
                            ],
                          );
                        },
                      );
                    }
                  }
                },
                child: const Text('Crear cuenta'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool isNumeric(String value) {
   
    return double.tryParse(value) != null;
  }
}
