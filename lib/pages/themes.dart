import 'package:flutter/cupertino.dart';
import 'package:blog/services/api_services.dart';
import 'package:blog/models/tema.dart';
import 'package:blog/pages/theme_details.dart'; // Importa la página ThemeDetailsPage

class ThemePage extends StatelessWidget {
  const ThemePage({super.key});

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Temas en mi blog'),
      ),
      child: FutureBuilder<List<Tema>>(
        future: ApiService.getThemes(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CupertinoActivityIndicator(),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Error: ${snapshot.error}'),
            );
          } else if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index) {
                Tema? tema = snapshot.data?[index];
                return CupertinoButton(
                  onPressed: () {
                    // Navegar a la página ThemeDetailsPage con el ID del tema
                    Navigator.push(
                      context,
                      CupertinoPageRoute(
                        builder: (context) => ThemeDetailsPage(temaId: tema.id),
                      ),
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: CupertinoColors.activeGreen, 
                      borderRadius: BorderRadius.circular(10.0), // Redondear las esquinas
                    ),
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        /*Text(
                          tema!.id.toString(), // Eliminar interpolación de cadenas
                          style: const TextStyle(color: CupertinoColors.white), // Color del texto blanco
                        ),*/
                        Flexible(
                          child: Text(
                            tema!.nombre, 
                            style: const TextStyle(color: CupertinoColors.white), // Color del texto blanco
                            overflow: TextOverflow.ellipsis, // Manejar desbordamiento de texto
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          } else {
            return const Center(
              child: Text('No se encontraron temas'),
            );
          }
        },
      ),
    );
  }
}
