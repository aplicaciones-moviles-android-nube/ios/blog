import 'package:flutter/cupertino.dart';
import 'package:blog/models/post.dart';
import 'package:blog/services/api_services.dart';
import 'package:intl/intl.dart';

class ArticleDetails extends StatefulWidget {
  final int postId;

  const ArticleDetails({super.key, required this.postId});

  @override
  ArticleDetailsState createState() => ArticleDetailsState();
}

class ArticleDetailsState extends State<ArticleDetails> {
  late Future<Post> _postFuture;

  @override
  void initState() {
    super.initState();
    _postFuture = ApiService.getPostById(widget.postId);
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(//crear  una pagina , se utiliza como widget principal en el método build
      navigationBar: const CupertinoNavigationBar(//con una barra de navegacion
        middle: Text('Detalles del artículo'),
      ),
      child: SafeArea(//garantiza que el contenido este visible y no se superponga
        child: FutureBuilder<Post>(
          future: _postFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (snapshot.hasError) {
              return const Center(
                child: Text('Error al cargar los detalles del artículo'),
              );
            } else {
              return _buildPostDetails(snapshot.data!);
            }
          },
        ),
      ),
    );
  }

  Widget _buildPostDetails(Post post) {
    DateTime fechaPublicacion = DateTime.parse(post.fechaPublicacion);
    String fechaFormateada = DateFormat('yyyy-MM-dd').format(fechaPublicacion);

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                post.titulo,
                style: const TextStyle(
                  fontSize: 20,
                  color: CupertinoColors.black,
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.none,
                ),
              ),
            ),
            Image.network(
              post.url,
              width: MediaQuery.of(context).size.width,
              height: 200,
              fit: BoxFit.cover,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                post.contenido,
                textAlign: TextAlign.justify,
                style: const TextStyle(
                  fontSize: 16.0,
                  color: CupertinoColors.black,
                  fontWeight: FontWeight.normal,
                  decoration: TextDecoration.none,
                  height: 1.5,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                'Fecha de publicación: $fechaFormateada',
                style: const TextStyle(
                  fontSize: 16,
                  color: CupertinoColors.black,
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.none,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                'Ranking: ${post.ranking}',
                style: const TextStyle(
                  fontSize: 16,
                  color: CupertinoColors.black,
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.none,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
