import 'package:flutter/cupertino.dart';
import 'package:blog/components/barra_inferior.dart'; 
import 'package:blog/pages/home_page.dart';
import 'package:blog/pages/login.dart'; 

class AboutMePage extends StatelessWidget {
  const AboutMePage({super.key});

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Acerca de mí'),
      ),
      child: SafeArea(
        child: SingleChildScrollView( // Envuelve el contenido con SingleChildScrollView
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // Contenido de la página
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // Foto centrada
                    Padding(
                      padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.1),
                      child: ClipOval(
                        child: Image.asset(
                          'assets/yo.png',
                          width: MediaQuery.of(context).size.width * 0.6,
                          height: MediaQuery.of(context).size.width * 0.6,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    // Párrafo con RichText
                    Padding(
                      padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.1),
                      child: RichText(
                        textAlign: TextAlign.justify,
                        text: const TextSpan(
                          style: TextStyle(
                            fontSize: 16.0,
                            color: CupertinoColors.black,
                            height: 1.5,
                          ),
                          children: <TextSpan>[
                            TextSpan(
                              text: '¡Hola! Soy Jonathan Castro, \n',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            TextSpan(
                              text: 'Me apasiona el mundo \n'
                                  'del desarrollo de software. Con experiencia tanto en el front-end como\n'
                                  'en el back-end, disfruto explorar las posibilidades que ofrece\n'
                                  'la tecnología para crear soluciones innovadoras. Actualmente, mi enfoque\n'
                                  'principal está en explorar flutter me parece atractivo para el desarrollo multiplataforma\n',
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              // Barra inferior
              BarraInferior(
                icono: const Icon(CupertinoIcons.home),
                onTapHome: () {
                  Navigator.pushReplacement(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => const HomePage(),
                    ),
                  );
                  Navigator.of(context).popUntil((route) => route.isFirst);
                },
                onTapGroup: () {
                  Navigator.pushReplacement(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => const LoginPage(),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
