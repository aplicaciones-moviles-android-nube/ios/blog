import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart'; // Importa el paquete para manejar la entrada de texto
import 'package:blog/models/post.dart';
import 'package:blog/services/api_services.dart';

class PostPage extends StatefulWidget {
  final Post? post;
  final bool isNewPost;

  const PostPage({super.key, this.post, required this.isNewPost});

  @override
  PostPageState createState() => PostPageState();
}

class PostPageState extends State<PostPage> {
  final TextEditingController _tituloController = TextEditingController();
  final TextEditingController _contenidoController = TextEditingController();
  final TextEditingController _urlController = TextEditingController();
  final TextEditingController _autorController = TextEditingController();
  final TextEditingController _rankingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (!widget.isNewPost && widget.post != null) {
      _tituloController.text = widget.post!.titulo;
      _contenidoController.text = widget.post!.contenido;
      _urlController.text = widget.post!.url;
      _autorController.text = widget.post!.autor.toString();
      _rankingController.text = widget.post!.ranking.toString();
    }
  }

  Future<void> _guardarPost() async {
    if (_tituloController.text.isEmpty ||
        _contenidoController.text.isEmpty ||
        _urlController.text.isEmpty ||
        _autorController.text.isEmpty ||
        _rankingController.text.isEmpty) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: const Text('Error'),
          content: const Text('Debes completar todos los campos.'),
          actions: [
            CupertinoDialogAction(
              child: const Text('Aceptar'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      );
      return;
    }

    final autor = int.tryParse(_autorController.text);
    if (autor == null) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: const Text('Error'),
          content: const Text('El ID del autor debe ser un número entero.'),
          actions: [
            CupertinoDialogAction(
              child: const Text('Aceptar'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      );
      return;
    }

    final ranking = int.tryParse(_rankingController.text);
    if (ranking == null) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: const Text('Error'),
          content: const Text('El ranking debe ser un número entero.'),
          actions: [
            CupertinoDialogAction(
              child: const Text('Aceptar'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      );
      return;
    }

    final Map<String, dynamic> postData = {
      "id": widget.post?.id ?? "default",
      "titulo": _tituloController.text,
      "contenido": _contenidoController.text,
      "url": _urlController.text,
      "autor": autor,
      "ranking": ranking,
    };

    try {
      bool success = widget.isNewPost
          ? await ApiService.createNewPost(postData)
          : await ApiService.updatePost(widget.post!.id, postData);

      if (success) {
        Navigator.pop(context);

        showCupertinoDialog(
          context: context,
          builder: (context) => CupertinoAlertDialog(
            title: const Text('Éxito'),
            content: Text(widget.isNewPost ? 'El post se publicó correctamente.' : 'El post se actualizó correctamente.'),
            actions: [
              CupertinoDialogAction(
                child: const Text('Aceptar'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        );
      } else {
        showCupertinoDialog(
          context: context,
          builder: (context) => CupertinoAlertDialog(
            title: const Text('Error'),
            content: const Text('Hubo un error al guardar el post.'),
            actions: [
              CupertinoDialogAction(
                child: const Text('Aceptar'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        );
      }
    } catch (e) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: const Text('Error'),
          content: Text('Hubo un error al guardar el post: $e'),
          actions: [
            CupertinoDialogAction(
              child: const Text('Aceptar'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text(widget.isNewPost ? 'Nuevo Post' : 'Actualizar Post'),
        trailing: CupertinoButton(
          onPressed: _guardarPost,
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: const Text(
            'Guardar',
            style: TextStyle(fontSize: 16),
          ),
        ),
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              CupertinoTextField(
                controller: _tituloController,
                placeholder: 'Título',
              ),
              const SizedBox(height: 10),
              Expanded(
                child: CupertinoTextField(
                  controller: _contenidoController,
                  placeholder: 'Contenido',
                  minLines: 5,
                  maxLines: null,
                ),
              ),
              const SizedBox(height: 10),
              CupertinoTextField(
                controller: _urlController,
                placeholder: 'URL de la imagen',
              ),
              const SizedBox(height: 10),
              CupertinoTextField(
                controller: _autorController,
                placeholder: 'ID del autor',
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly // Limita la entrada a solo números
                ],
              ),
              const SizedBox(height: 10),
              CupertinoTextField(
                controller: _rankingController,
                placeholder: 'Ranking',
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly // Limita la entrada a solo números
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
