import 'package:flutter/cupertino.dart';
import 'package:blog/models/post.dart';
import 'package:blog/services/api_services.dart';
import 'package:blog/pages/post.dart';
import 'package:blog/pages/assign_topic.dart';

class ListPosts extends StatefulWidget {
  const ListPosts({super.key});

  @override
  ListPostsState createState() => ListPostsState();
}

//El estado es la información que puede cambiar durante la vida útil de un widget
//mientras que los widgets con estado pueden tener datos internos que cambian, 
//lo que afecta cómo se muestra el widget en la interfaz de usuario
class ListPostsState extends State<ListPosts> {
  late Future<List<Post>> _futurePosts;
  bool hasPerformedAction = false;

  @override
  void initState() {
    super.initState();
    _futurePosts = ApiService.getAllPosts();
  }

  Future<void> _refreshPosts() async {
    setState(() {
      _futurePosts = ApiService.getAllPosts();
    });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: const Text('Lista de Publicaciones'),
        trailing: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            CupertinoButton(
              padding: EdgeInsets.zero,
              onPressed: () {
                Navigator.of(context).push(
                  CupertinoPageRoute(
                    builder: (context) {
                      return FutureBuilder<List<Post>>(
                        future: _futurePosts,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return const Center(
                              child: CupertinoActivityIndicator(),
                            );
                          } else if (snapshot.hasError) {
                            return Center(
                              child: Text('Error: ${snapshot.error}'),
                            );
                          } else {
                            final List<Post> posts = snapshot.data!;
                            return PostPage(
                              post: posts.isNotEmpty
                                  ? posts[0]
                                  : Post(
                                      id: 0,
                                      titulo: '',
                                      contenido: '',
                                      url: '',
                                      fechaPublicacion: '',
                                      autor: 0,
                                      ranking: 0,
                                    ),
                              isNewPost: !hasPerformedAction,
                            );
                          }
                        },
                      );
                    },
                  ),
                );
              },
              child: hasPerformedAction
                  ? const Text('Actualizar Post')
                  : const Icon(CupertinoIcons.add),
            ),
            CupertinoButton(
              padding: EdgeInsets.zero,
              onPressed: () {
                Navigator.of(context).push(
                  CupertinoPageRoute(
                    builder: (context) => const AssignTopicPage(),
                  ),
                );
              },
              child: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(width: 4),
                  Text('Asignar Tema'),
                ],
              ),
            ),
          ],
        ),
      ),
      child: SafeArea(
        child: MediaQuery(
          data: MediaQuery.of(context),
          child: CupertinoScrollbar(
            child: CustomScrollView(
              slivers: [
                CupertinoSliverRefreshControl(
                  onRefresh: _refreshPosts,
                ),
                FutureBuilder<List<Post>>(
                  future: _futurePosts,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const SliverFillRemaining(
                        child: Center(
                          child: CupertinoActivityIndicator(),
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return SliverFillRemaining(
                        child: Center(
                          child: Text('Error: ${snapshot.error}'),
                        ),
                      );
                    } else {
                      final List<Post> posts = snapshot.data!;
                      return SliverList(
                        delegate: SliverChildBuilderDelegate(
                          (context, index) {
                            final post = posts[index];
                            return _buildTableRow(context, post);
                          },
                          childCount: posts.length,
                        ),
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTableRow(BuildContext context, Post post) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          _buildTableCell(
            'ID: ${post.id}',
            const TextStyle(
              fontSize: 20,
              color: CupertinoColors.black,
              fontWeight: FontWeight.bold,
              decoration: TextDecoration.none,
            ),
          ),
          _buildTableCell(
            'Título: ${post.titulo}',
            const TextStyle(
              fontSize: 18,
              color: CupertinoColors.systemBlue,
              fontWeight: FontWeight.normal,
              decoration: TextDecoration.none,
            ),
          ),
          _buildTableCell(
            'Ranking: ${post.ranking}',
            const TextStyle(
              fontSize: 14,
              color: CupertinoColors.systemGrey,
              fontWeight: FontWeight.normal,
              decoration: TextDecoration.none,
            ),
          ),
          _buildTableCell(
            'Fecha de Publicación: ${post.fechaPublicacion}',
            const TextStyle(
              fontSize: 14,
              color: CupertinoColors.systemGrey,
              fontWeight: FontWeight.normal,
              decoration: TextDecoration.none,
            ),
          ),
          _buildActionButtons(context, post),
        ],
      ),
    );
  }

  Widget _buildActionButtons(BuildContext context, Post post) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        CupertinoButton(
          padding: EdgeInsets.zero,
          onPressed: () {
            Navigator.of(context).push(
              CupertinoPageRoute(
                builder: (context) => PostPage(
                  post: post,
                  isNewPost: false,
                ),
              ),
            );
          },
          child: const Icon(CupertinoIcons.pencil),
        ),
        CupertinoButton(
          padding: EdgeInsets.zero,
          onPressed: () {
            _showDeleteConfirmationDialog(context, post);
          },
          child: const Icon(CupertinoIcons.trash),
        ),
      ],
    );
  }

  Widget _buildTableCell(String text, TextStyle style) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        text,
        style: style,
        textAlign: TextAlign.center,
      ),
    );
  }
}

void _showDeleteConfirmationDialog(BuildContext context, Post post) {
  showCupertinoDialog(
    context: context,
    builder: (context) {
      return CupertinoAlertDialog(
        title: const Text('Confirmación'),
        content: const Text('¿Estás seguro de que quieres eliminar este post?'),
        actions: [
          CupertinoDialogAction(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text('Cancelar'),
          ),
          CupertinoDialogAction(
            onPressed: () async {
              await _deletePost(context, post.id);
              Navigator.of(context)
                  .popUntil((route) => route is! CupertinoDialogRoute);
            },
            isDestructiveAction: true,
            child: const Text('Eliminar'),
          ),
        ],
      );
    },
  );
}

Future<void> _deletePost(BuildContext context, int postId) async {
  try {
    final bool deleted = await ApiService.deletePost(postId);
    if (deleted) {
      showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: const Text('Éxito'),
            content: const Text('El post se eliminó correctamente.'),
            actions: [
              CupertinoDialogAction(
                onPressed: () {
                  Navigator.of(context)
                      .popUntil((route) => route is! CupertinoDialogRoute);
                },
                child: const Text('OK'),
              ),
            ],
          );
        },
      );
    } else {
      showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: const Text('Error'),
            content: const Text('No se pudo eliminar el post.'),
            actions: [
              CupertinoDialogAction(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text('OK'),
              ),
            ],
          );
        },
      );
    }
  } catch (e) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: const Text('Error'),
          content: Text('Error al eliminar el post: $e'),
          actions: [
            CupertinoDialogAction(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('OK'),
            ),
          ],
        );
      },
    );
  }
}
