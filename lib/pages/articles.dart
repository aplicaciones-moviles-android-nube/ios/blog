import 'package:flutter/cupertino.dart';
import 'package:blog/components/barra_inferior.dart';
import 'package:blog/models/post.dart';
import 'package:blog/pages/article_details.dart';
import 'package:blog/pages/home_page.dart';
import 'package:blog/pages/login.dart';

class ArticlePage extends StatelessWidget {
  final Future<List<Post>> postsFuture;

  const ArticlePage({super.key, required this.postsFuture});

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Artículos'),
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0), // Padding alrededor de todo el contenido
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // Contenido de la página
              Expanded(
                child: FutureBuilder<List<Post>>(
                  future: postsFuture,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Center(
                        child: CupertinoActivityIndicator(),
                      );
                    } else if (snapshot.hasError) {
                      return const Center(
                        child: Text('Error al cargar los datos'),
                      );
                    } else {
                      List<Post> posts = snapshot.data ?? [];
                      return CupertinoScrollbar(
                        child: ListView.builder(
                          itemCount: posts.length,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  CupertinoPageRoute(
                                    builder: (context) =>
                                        ArticleDetails(postId: posts[index].id),
                                  ),
                                );
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: CupertinoColors.white,
                                    borderRadius: BorderRadius.circular(12.0),
                                    boxShadow: [
                                      BoxShadow(
                                        color: CupertinoColors.systemGrey.withOpacity(0.5),
                                        blurRadius: 5,
                                        offset: const Offset(0, 3),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          posts[index].titulo,
                                          style: const TextStyle(
                                            fontSize: 20,
                                            color: CupertinoColors.black,
                                            fontWeight: FontWeight.bold,
                                            decoration: TextDecoration.none,
                                          ),
                                        ),
                                      ),
                                      Image.network(
                                        posts[index].url,
                                        width: double.infinity,
                                        height: 200,
                                        fit: BoxFit.cover,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      );
                    }
                  },
                ),
              ),
              // Barra inferior
              BarraInferior(
                icono: const Icon(CupertinoIcons.home),
                onTapHome: () {
                  Navigator.pushReplacement(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => const HomePage(),
                    ),
                  );
                  // Eliminar todas las rutas de navegación anteriores
                  Navigator.of(context).popUntil((route) => route.isFirst);
                },
                onTapGroup: () {
                  Navigator.pushReplacement(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => const LoginPage(),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
