import 'package:flutter/cupertino.dart';
import 'package:blog/models/post.dart';
import 'package:blog/services/api_services.dart';
import 'package:intl/intl.dart';

class ThemeDetailsPage extends StatelessWidget {
  final int temaId;

  const ThemeDetailsPage({super.key, required this.temaId});

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Detalles del Tema'),
      ),
      child: SafeArea(
        child: FutureBuilder<List<Post>>(
          future: ApiService.getPostsForTheme(temaId),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CupertinoActivityIndicator(),
              );
            } else if (snapshot.hasError) {
              return const Center(
                child: Text('Error al cargar los detalles del tema'),
              );
            } else if (snapshot.hasData && snapshot.data!.isNotEmpty) {
              return ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: _buildPostDetails(snapshot.data![index]),
                  );
                },
              );
            } else {
              return const Center(
                child: Text('No se encontraron detalles del tema'),
              );
            }
          },
        ),
      ),
    );
  }

  Widget _buildPostDetails(Post post) {
    DateTime fechaPublicacion = DateTime.parse(post.fechaPublicacion);
    String fechaFormateada = DateFormat('yyyy-MM-dd').format(fechaPublicacion);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          post.titulo,
          style: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 10),
        Image.network(
          post.url,
          width: double.infinity,
          height: 200,
          fit: BoxFit.cover,
        ),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Flexible(
            child: Text(
              post.contenido,
              textAlign: TextAlign.justify,
              style: const TextStyle(
                fontSize: 16.0,
                height: 1.5,
              ),
            ),
          ),
        ),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Text(
            'Fecha de publicación: $fechaFormateada',
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        const SizedBox(height: 5),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Text(
            'Ranking: ${post.ranking}',
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}
