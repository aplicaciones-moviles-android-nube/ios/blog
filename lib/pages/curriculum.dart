import 'package:flutter/cupertino.dart';

class CupertinoCurriculumPage extends StatelessWidget {

    const CupertinoCurriculumPage({super.key});
     //Los widgets sin estado no tienen datos internos
  @override
  Widget build(BuildContext context) {
    return const CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Curriculum Vitae'),
      ),
      child: SafeArea(
        child: Padding(
          
          padding:  EdgeInsets.all(20.0),
            child: CupertinoScrollbar(
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Mis Datos',
                      style:  TextStyle(
                        fontSize: 20.0,
                        color: CupertinoColors.activeBlue,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Text('Nombre: Jonathan Leo Castro García', style:  TextStyle(
                      fontSize: 14.0,
                      color: CupertinoColors.black,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none,
                    ),),
                    Text('Ubicación: San Francisco del Rincón, Gto', style:  TextStyle(
                      fontSize: 14.0,
                      color: CupertinoColors.black,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none,
                    ),),
                    Text('RFC: CAGJ841210E7A', style:  TextStyle(
                      fontSize: 14.0,
                      color: CupertinoColors.black,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none,
                    ),),
                    Text('Fecha de nacimiento: 10 Diciembre de 1984', style:  TextStyle(
                      fontSize: 14.0,
                      color: CupertinoColors.black,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none,
                    ),),
                    Text('Teléfono: 4761135095', style:  TextStyle(
                      fontSize: 14.0,
                      color: CupertinoColors.black,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none,
                    ),),
                    Text('Email: jonathanctro@outlook.com', style:  TextStyle(
                      fontSize: 14.0,
                      color: CupertinoColors.black,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none,
                    ),),
                    SizedBox(height: 20.0),
                    Text(
                      'Acerca de mí',
                      style:  TextStyle(
                        fontSize: 20.0,
                        color: CupertinoColors.activeBlue,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      'Me apasiona aprender nuevas tecnologías, especialmente en el campo del desarrollo web. Se ha convertido en la corriente principal en la industria del software. Me considero una persona autodidacta, siempre buscando oportunidades para mejorar mis habilidades y conocimientos en este emocionante campo. Disfruto compartiendo y conversando sobre tecnología. Me encanta trabajar en equipo y colaborar para alcanzar las metas establecidas. Creo firmemente en el poder del trabajo en equipo y cómo puede impulsar el éxito en los proyectos. Mi objetivo es seguir creciendo en el área de tecnología y estar actualizado con las últimas tendencias.',
                      style:  TextStyle(
                        fontSize: 14.0,
                        color: CupertinoColors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Text(
                      'Formación Académica',
                      style:  TextStyle(
                        fontSize: 20.0,
                        color: CupertinoColors.activeBlue,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      '2003-2007: Universidad de la Salle Bajío, León - Licenciatura en Ingeniería en Computación y Sistemas',
                      style:  TextStyle(
                        fontSize: 14.0,
                        color: CupertinoColors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      'Otros Títulos y Seminarios',
                      style:  TextStyle(
                        fontSize: 20.0,
                        color: CupertinoColors.activeBlue,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      '2004-2005: Diplomado en Linux (168 horas) por la Universidad De La Salle Bajío',
                      style:  TextStyle(
                        fontSize: 14.0,
                        color: CupertinoColors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                    Text(
                      '2007: Sql server 2005 (20 horas) por el Tecnológico de Monterrey Campus León',
                      style:  TextStyle(
                        fontSize: 14.0,
                        color: CupertinoColors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                    // Agrega aquí el resto de tus títulos y seminarios
                    SizedBox(height: 20.0),
                    Text(
                      'Experiencia Laboral',
                      style:  TextStyle(
                        fontSize: 20.0,
                        color: CupertinoColors.activeBlue,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      '2007: Laboratorios de la Universidad De La Salle Bajío, León,Gto - Desarrollo de esquema de seguridad Integrando firewall (ISA server, fwbuilder).',
                      style:  TextStyle(
                        fontSize: 14.0,
                        color: CupertinoColors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                    Text(
                      '2009-2010: Coopdesarrollo SC de AP de RL de CV, San Francisco del Rincón,Gto- Operador de procesos - Desarrollo de Intranet.',
                      style:  TextStyle(
                        fontSize: 14.0,
                        color: CupertinoColors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                     Text(
                      '2010: Coopdesarrollo SC de AP de RL de CV, San Francisco del Rincón- Operador de proceso - Desarrollo de sitio web',
                      style:  TextStyle(
                        fontSize: 14.0,
                        color: CupertinoColors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                     ),
                     Text(
                      '2010: Coopdesarrollo SC de AP de RL de CV, San Francisco del Rincón-Operador de proceso - Operación de servidor IBM i para manejar el core bancario de la Institución',
                      style:  TextStyle(
                        fontSize: 14.0,
                        color: CupertinoColors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                     ),
                      Text(
                      '2010: Coopdesarrollo SC de AP de RL de CV, San Francisco del Rincón-Operador de proceso -  Participación en desarrollo de Sistema de reportes regulatorios',
                      style:  TextStyle(
                        fontSize: 14.0,
                        color: CupertinoColors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                     ),
                     Text(
                      '2013-2014 Coopdesarrollo SC de AP de RL de CV, San Francisco del Rincón-Jefe desarrollo de sistemas -  Desarrollo y actualizaciones de los módulo s de cartera vigente, cartera vencida  en sistema de cobranza para Coopdesarrollo  S.C.L de AP de RL de C.V',
                      style:  TextStyle(
                        fontSize: 14.0,
                        color: CupertinoColors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                     ),
                      Text(
                      '2015-2021 Coopdesarrollo SC de AP de RL de CV, San Francisco del Rincón-Jefe desarrollo de sistemas - Análisis, Desarrollo de nuevos proyectos, actualización y soporte a las aplicaciones en lenguaje Visual Basic .Net para Coopdesarrollo S.C.L de AP de RL de C.V',
                      style:  TextStyle(
                        fontSize: 14.0,
                        color: CupertinoColors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                     ),

                      Text(
                      '2022-2024 SAPAF (Sistema de Agua Potable de San Francisco del Rincón)-Jefe de Informática - Análisis,Encargado de Mantenimiento al Sistema Informático Core,Desarrollando front con  vue y Backend (Api) en con C#. ',
                      style:  TextStyle(
                        fontSize: 14.0,
                        color: CupertinoColors.black,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                     ),


                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ))
    ;
  }
}
