import 'package:flutter/cupertino.dart';
import 'package:blog/pages/login.dart';
import 'package:blog/pages/list_posts.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({super.key});

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(//proporcina una estructura basica para una pagina, con una barra de navegacion en la parte superior y area de contenido centro
      navigationBar: CupertinoNavigationBar(
        middle: const Text(
          'Dashboard',
          style: TextStyle(
            fontSize: 16.0,
            color: CupertinoColors.black,
            fontWeight: FontWeight.normal,
            decoration: TextDecoration.none,
            height: 1.5,
          ),
        ),
        trailing: CupertinoButton(
          padding: EdgeInsets.zero,
          child: const Text('Cerrar Sesión',
              style: TextStyle(
                fontSize: 16,
                color: CupertinoColors.black,
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.none,
              )),
          onPressed: () {
            // Navega de regreso a la página de inicio de sesión
            Navigator.pushAndRemoveUntil(
              context,
              CupertinoPageRoute(builder: (context) => const LoginPage()),
              (route) =>
                  false, // Elimina todas las rutas restantes del historial
            );
          },
        ),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              '¡Bienvenido al dashboard!',
              style: TextStyle(
                fontSize: 24.0,
                color: CupertinoColors.black,
                fontWeight: FontWeight.normal,
                decoration: TextDecoration.none,
                height: 1.5,
              ),
            ),
            const SizedBox(height: 100),
            CupertinoButton(
              onPressed: () {
                // Navega a la página de lista de publicaciones
                Navigator.push(
                  context,
                  CupertinoPageRoute(builder: (context) => const ListPosts()),
                );
              },
              color: CupertinoColors.activeBlue,
              child: const Text(
                'Lista de posts',
                style: TextStyle(
                  color: CupertinoColors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const SizedBox(height: 20), // Espacio entre las tarjetas
          ],
        ),
      ),
    );
  }
}
