import 'package:flutter/cupertino.dart';
import 'package:blog/services/api_services.dart';
import 'package:blog/models/post.dart';
import 'package:intl/intl.dart';//libreria para formatear la fecha del post

class RankedArticles extends StatelessWidget {
  const RankedArticles({super.key});

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Artículos más Rankeados'),
      ),
      child: SafeArea(
        child: FutureBuilder<List<Post>>(
          future: fetchRankedPost(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CupertinoActivityIndicator());
            } else if (snapshot.hasError) {
              // Manejar el error
              return Center(
                child: Text('Error: ${snapshot.error}'),
              );
            } else {
              List<Post> posts = snapshot.data ?? [];
              return CupertinoScrollbar(
                child: ListView.builder(
                  itemCount: posts.length,
                  itemBuilder: (context, index) {
                    DateTime fechaPublicacion =
                        DateTime.parse(posts[index].fechaPublicacion);
                    String fechaFormateada =
                        DateFormat('yyyy-MM-dd').format(fechaPublicacion);
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            posts[index].titulo,
                            style: const TextStyle(
                              fontSize: 20,
                              color: CupertinoColors.black,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.none,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: Image.network(
                              posts[index].url,
                              width: double.infinity,
                              height: 300,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Flexible(
                            child: RichText(
                              textAlign: TextAlign.justify,
                              text: TextSpan(
                                text: posts[index].contenido,
                                style: const TextStyle(
                                  fontSize: 16.0,
                                  color: CupertinoColors.black,
                                  height: 1.5,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Fecha de publicación: $fechaFormateada',
                            textAlign: TextAlign.left,
                            style: const TextStyle(
                              fontSize: 16,
                              color: CupertinoColors.black,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.none,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8.0, 0, 0, 0),
                          child: Text(
                            'Ranking: ${posts[index].ranking}',
                            textAlign: TextAlign.left,
                            style: const TextStyle(
                              fontSize: 16,
                              color: CupertinoColors.black,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.none,
                            ),
                          ),
                        ),
                        const SizedBox(height: 50.0),
                      ],
                    );
                  },
                ),
              );
            }
          },
        ),
      ),
    );
  }

  Future<List<Post>> fetchRankedPost() async {
    final List<Post> posts = await ApiService.getRankedPosts();
    return posts;
  }
}
