import 'package:flutter/cupertino.dart';
import 'package:blog/pages/dash_board.dart';
import 'package:blog/components/navigation_options.dart';
import 'package:blog/services/api_services.dart';
import 'package:blog/components/barra_inferior.dart';
import 'package:blog/pages/home_page.dart';
import 'package:blog/pages/create_account.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _usuarioController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Row(
          children: [
            // logo
            Image.asset(
              'assets/logo.jpg',
              width: MediaQuery.of(context).size.width * 0.3,
              height: MediaQuery.of(context).size.width * 0.25,
            ),
            const SizedBox(width: 8),
            const Text(
              'Mi Blog',
              style: TextStyle(fontSize: 20),
            ),
          ],
        ),
        trailing: const NavigationOptions(),
      ),
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      const Text(
                        'Bienvenido',
                        style: TextStyle(
                          fontSize: 20,
                          color: CupertinoColors.black,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.none,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(height: 20),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.2,
                        height: MediaQuery.of(context).size.width * 0.2,
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/blog.webp'),
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      CupertinoTextField(
                        controller: _usuarioController,
                        placeholder: 'Correo Electrónico',
                        keyboardType: TextInputType.emailAddress,
                        padding: const EdgeInsets.all(12.0),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: CupertinoColors.lightBackgroundGray,
                          ),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),
                      const SizedBox(height: 20),
                      CupertinoTextField(
                        controller: _passwordController,
                        placeholder: 'Contraseña',
                        obscureText: true,
                        padding: const EdgeInsets.all(12.0),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: CupertinoColors.lightBackgroundGray,
                          ),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),
                      const SizedBox(height: 20),
                      CupertinoButton.filled(
                        onPressed: _signIn,
                        child: const Text('Iniciar Sesión'),
                      ),
                      const SizedBox(height: 10),
                      CupertinoButton(
                        onPressed: () {
                          // Navegar a la página de registro
                          Navigator.push(
                            context,
                            CupertinoPageRoute(
                              builder: (context) => const CreateAccountPage(),
                            ),
                          );
                        },
                        child: const Text('Crear una cuenta'),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            BarraInferior(
              icono: const Icon(CupertinoIcons.home),
              onTapHome: () {
                Navigator.pushReplacement(
                  context,
                  CupertinoPageRoute(
                    builder: (context) => const HomePage(),
                  ),
                );
                // Eliminar todas las rutas de navegación anteriores
                Navigator.of(context).popUntil((route) => route.isFirst);
              },
              onTapGroup: () {
                Navigator.pushReplacement(
                  context,
                  CupertinoPageRoute(
                    builder: (context) => const LoginPage(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  void _signIn() async {
    // Obtener los valores del correo electrónico y la contraseña
    String email = _usuarioController.text;
    String password = _passwordController.text;

    // Validar si los campos no están vacíos
    if (email.isEmpty || password.isEmpty) {
      _showMessage('Error', 'Por favor, complete todos los campos.');
      return;
    }

    // Autenticar al usuario utilizando ApiService
    bool isAuthenticated = await ApiService.authenticateUser(email, password);

    if (isAuthenticated) {
      // Si las credenciales son válidas, navegar a la pantalla del dashboard
      Navigator.pushReplacement(
        context,
        CupertinoPageRoute(
          builder: (context) => const DashboardPage(),
        ),
      );
    } else {
      // Si las credenciales no son válidas, mostrar un mensaje de error
      _showMessage('Error', 'Correo electrónico o contraseña incorrectos.');
    }
  }

  void _showMessage(String title, String message) {
    showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(message),
        actions: [
          CupertinoDialogAction(
            child: const Text('Aceptar'),
            onPressed: () {
              Navigator.pop(context); // Cerrar el diálogo
            },
          ),
        ],
      ),
    );
  }
}
