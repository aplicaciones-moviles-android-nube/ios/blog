import 'package:flutter/cupertino.dart';
import 'package:blog/components/navigation_options.dart';
import 'package:blog/components/barra_inferior.dart';
import 'package:blog/services/api_services.dart';
import 'package:blog/models/post.dart';
import 'package:blog/pages/ranked_articles.dart';
import 'package:blog/pages/curriculum.dart';
import 'package:blog/pages/login.dart';
import 'package:blog/pages/themes.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  Future<Post?> _fetchPost() async {
    try {
      Post post = await ApiService.getPostById(5);
      return post;
    } catch (error) {
      throw Exception('Falla al cargar el post');
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(//es un widget en Flutter que proporciona una estructura básica para una página con una barra de navegación en la parte superior y un área de contenido en el centro.
      navigationBar: CupertinoNavigationBar(
        middle: Row(
          children: [
            Image.asset(
              'assets/logo.jpg',
              width: 120,
              height: 100,
            ),
            const SizedBox(width: 8),
            const Text(
              'Mi Blog',
              style: TextStyle(fontSize: 20),
            ),
          ],
        ),
        trailing: const NavigationOptions(),
      ),
      child: SafeArea(
        child: FutureBuilder<Post?>(
          future: _fetchPost(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(child: CupertinoActivityIndicator());
            } else if (snapshot.hasError) {
              return Center(child: Text('Error: ${snapshot.error}'));
            } else if (!snapshot.hasData) {
              return const Center(child: Text('No se encontraron posts'));
            } else {
              Post post = snapshot.data!;

              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 20),
                  Expanded(
                    child: ListView.builder(
                      itemCount: 1,
                      itemBuilder: (context, index) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                alignment: Alignment.center,
                                child: Text(
                                  post.titulo,
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    fontSize: 20,
                                    color: CupertinoColors.black,
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.none,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10.0), // Redondear las esquinas
                                child: Image.network(
                                  post.url,
                                  width: double.infinity,
                                  height: 200,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Center(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    CupertinoButton.filled(
                                      onPressed: () {
                                        Navigator.of(context).push(
                                          CupertinoPageRoute(
                                            builder: (context) => const ThemePage(),
                                          ),
                                        );
                                      },
                                      child: const Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Icon(CupertinoIcons.news_solid, size: 20),
                                          SizedBox(width: 8),
                                          Text('Temas en el blog'),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(height: 20),
                                    CupertinoButton.filled(
                                      onPressed: () {
                                        Navigator.of(context).push(
                                          CupertinoPageRoute(
                                            builder: (context) => const RankedArticles(),
                                          ),
                                        );
                                      },
                                      child: const Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Icon(CupertinoIcons.book_circle, size: 20),
                                          SizedBox(width: 8),
                                          Text('Art. Ranqueados'),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(height: 20),
                                    CupertinoButton.filled(
                                      onPressed: () {
                                        Navigator.of(context).push(
                                          CupertinoPageRoute(
                                            builder: (context) => const CupertinoCurriculumPage(),
                                          ),
                                        );
                                      },
                                      child: const Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Icon(CupertinoIcons.news, size: 20),
                                          SizedBox(width: 8),
                                          Text('Curriculum Vitae'),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                  const SizedBox(height: 20),
                  // BarraInferior componente reutilizable
                  BarraInferior(
                    icono: const Icon(CupertinoIcons.home),
                    onTapHome: () {
                      Navigator.pushReplacement(
                        context,
                        CupertinoPageRoute(builder: (context) => const HomePage()),
                      );
                      Navigator.of(context).popUntil((route) => route.isFirst);
                    },
                    onTapGroup: () {
                      Navigator.pushReplacement(
                        context,
                        CupertinoPageRoute(builder: (context) => const LoginPage()),
                      );
                    },
                  ),
                  // Agregamos un SizedBox para que la barra inferior esté anclada al final de la pantalla
                  SizedBox(height: MediaQuery.of(context).padding.bottom),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}
