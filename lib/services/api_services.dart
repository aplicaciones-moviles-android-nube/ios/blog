import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:blog/models/post.dart';
import 'package:blog/models/tema.dart';
import 'package:blog/models/post_tema.dart';

class ApiService {
  //static const String baseUrl = 'http://localhost:8888';
  static const String baseUrl = 'http://api-final.hopto.org:8888';

  //Obtiene todo los posts
  static Future<List<Post>> getAllPosts() async {
    final response = await http.get(Uri.parse('$baseUrl/posts'));

    if (response.statusCode == 200) {
      //decodifica el cuerpo de la respuesta JSON, en una lista de objetos dinamicos
      List<dynamic> jsonResponse = jsonDecode(response.body);
      //Luego mapea esta lista de objetos dinamicos a una lista de objetos de tipo Post
      return jsonResponse.map((data) => Post.fromJson(data)).toList();
    } else {
      throw Exception('Error al cargar los posts');
    }
  }

  //obtener post pot id
  static Future<Post> getPostById(int postId) async {
    final response = await http.get(Uri.parse('$baseUrl/posts/$postId'));

    if (response.statusCode == 200) {
      // como solo es un valor decodificamos y mapeamos la respuesta en clave(string) valor
      Map<String, dynamic>? jsonResponse = jsonDecode(response.body);
      if (jsonResponse != null) {
        // print('JSON response: $jsonResponse'); // Imprime el JSON de la respuesta
        return Post.fromJson(jsonResponse);
      } else {
        // print('El JSON de la respuesta está vacío o es nulo');
        throw Exception('Vacio or null respuesta JSON');
      }
    } else {
      throw Exception('Falla al cargar el post');
    }
  }

  //Obtenemos la lista de los post mas Rankeados
  static Future<List<Post>> getRankedPosts() async {
    final response = await http.get(Uri.parse('$baseUrl/posts'));
    if (response.statusCode == 200) {
      final List<dynamic> responseData = json.decode(response.body);
      final List<Post> posts = [];
      for (var data in responseData) {
        // Filtrar publicaciones con rating mayor a 100
        if (data['ranking'] >= 100) {
          posts.add(Post.fromJson(data));
        }
      }
      return posts;
    } else {
      throw Exception('Error al carga los post rankeados');
    }
  }

  //metodo para autenticar usuario
  static Future<bool> authenticateUser(String email, String password) async {
    final url = Uri.parse('$baseUrl/login');
    final body = jsonEncode({'correoElectronico': email, 'password': password});
    final headers = {'Content-Type': 'application/json'};

    final response = await http.post(url, headers: headers, body: body);

    if (response.statusCode == 200) {
      // Si la respuesta es 200, significa que el usuario se autenticó correctamente
      return true;
    } else {
      return false;
    }
  }

  //Crear un nuevo post
  static Future<bool> createNewPost(Map<String, dynamic> postData) async {
    final url = Uri.parse('$baseUrl/nuevo-post');
    final body = jsonEncode(postData);
    final headers = {'Content-Type': 'application/json'};

    final response = await http.post(url, headers: headers, body: body);

    if (response.statusCode == 200) {
      // Si la respuesta es 200, significa que el post se creó correctamente
      return true;
    } else {
      // Si hay algún error en la solicitud, lanza una excepción
      throw Exception('Falla al crear el post');
    }
  }

  //actuaizacion del post
  static Future<bool> updatePost(
      int postId, Map<String, dynamic> postData) async {
    final url = Uri.parse('$baseUrl/actualiza-post/$postId');
    final body = jsonEncode(postData);
    final headers = {'Content-Type': 'application/json'};

    final response = await http.put(url, headers: headers, body: body);

    if (response.statusCode == 200) {
      // Si la respuesta es 200, significa que el post se actualizó correctamente
      return true;
    } else {
      // Si hay algún error en la solicitud, lanza una excepción
      throw Exception('Falla al actualizar el post');
    }
  }

  //borrado del post
  static Future<bool> deletePost(int postId) async {
    final url = Uri.parse('$baseUrl/borra-post/$postId');
    final response = await http.delete(url);

    if (response.statusCode == 200) {
      // Si la respuesta es 200, significa que el post se eliminó correctamente
      return true;
    } else {
      // Si hay algún error en la solicitud, lanza una excepción
      throw Exception('Falla al borrar el post');
    }
  }

  //Crea nuevo usuario
  static Future<bool> createNewUser(Map<String, dynamic> userData) async {
    final url = Uri.parse('$baseUrl/nuevo-usuario');
    final body = jsonEncode(userData);
    final headers = {'Content-Type': 'application/json'};

    final response = await http.post(url, headers: headers, body: body);

    if (response.statusCode == 200) {
      // Si la respuesta es 200, significa que el usuario se creó correctamente
      return true;
    } else {
      // Si hay algún error en la solicitud, lanza una excepción
      throw Exception('Falla al crear el usuario');
    }
  }

  //Obtener post por temas
  static Future<List<Tema>> getThemes() async {
    final response = await http.get(Uri.parse('$baseUrl/temas'));

    if (response.statusCode == 200) {
      List<dynamic> jsonResponse = jsonDecode(response.body);
      return jsonResponse.map((data) => Tema.fromJson(data)).toList();
    } else {
      throw Exception('Error al cargar los temas');
    }
  }

  //Obtiene los post por tema
  static Future<List<Post>> getPostsForTheme(int themeId) async {
    final response = await http.get(Uri.parse('$baseUrl/temas/$themeId'));

    if (response.statusCode == 200) {
      List<dynamic> jsonResponse = jsonDecode(response.body);
      // print("Salida de jsonResponse: $jsonResponse");
      List<Post> posts =
          jsonResponse.map((data) => Post.fromJson(data)).toList();
      return posts;
    } else {
      throw Exception('Fallo al cargar los posts del tema');
    }
  }

  //insertar los temas en la tabla relacional post_temas
  static Future<PostTema> assignTopic(int idPost, int idTema) async {
    // Crear el objeto PostTema con los IDs proporcionados
    final postTema = PostTema(id: 0, postId: idPost, temaId: idTema);

    try {
      // Convertir el objeto a JSON
      final jsonBody = jsonEncode(postTema.toJson());

      // Imprimir el JSON antes de enviarlo (para depuración)
      //print('JSON enviado a assign_topic: $jsonBody');

      // Realizar la solicitud HTTP
      final response = await http.post(
        Uri.parse('$baseUrl/temas/insert-relate-themes'),
        body: jsonBody,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      );

      // Verificar si la solicitud fue exitosa
      if (response.statusCode == 200) {
        // Si la respuesta fue exitosa, devolver el objeto PostTema creado
        return PostTema.fromJson(jsonDecode(response.body));
      } else {
        // Si la respuesta no fue exitosa, lanzar una excepción con un mensaje de error
        throw Exception(
            'Error al asignar el tema al post. Código de estado: ${response.statusCode}');
      }
    } catch (e) {
      // Capturar cualquier excepción que ocurra durante el proceso y lanzarla de nuevo
      throw Exception('Error al realizar la solicitud HTTP: $e');
    }
  }
}
